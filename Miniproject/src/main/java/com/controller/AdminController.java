package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bean.Admin;
import com.service.AdminService;

@Controller
public class AdminController {
	@Autowired
	AdminService adminDao;
	
	@GetMapping(value="start")
	public String openInsex() {
		return "index";
	}
	@GetMapping(value="admin")
	public String openAdminPage() {
		return "admin";
	}
	@GetMapping(value = "user")
	public String openUserloginpage() {
		return "userlogin";
	}
	@GetMapping(value = "register")
	public String openUserRegistrationPage() {	
		return "userregistration";
	}
	@GetMapping(value="admincheck")
	public String checkAdmin(HttpServletRequest req,HttpSession hs) {
		String email=req.getParameter("email");
		String password=req.getParameter("pass");
		Admin admn=new Admin();
		admn.setEmail(email);
		admn.setPassword(password);
		String res=adminDao.checkAdminDetails(admn);
		if(res.equalsIgnoreCase("sucess")) {
			return "AdminHome";
		}else {
			hs.setAttribute("ad1","please enter valid details");
			return "admin";
		}
	}

	@GetMapping(value="logout")
	public String adminLogout() {
		return"logout";
	}


}
