package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Items {
	@Id
	private int item_Id;
	private String item_Name;
	private String item_Type;
	private float item_Price;
	public Items() {
		super();
	}
	public Items(int itemId, String itemName, String itemType, float itemPrice) {
		super();
		this.item_Id = itemId;
		this.item_Name = itemName;
		this.item_Type = itemType;
		this.item_Price = itemPrice;
	}
	public int getItemId() {
		return item_Id;
	}
	public void setItemId(int itemId) {
		this.item_Id = itemId;
	}
	public String getItemName() {
		return item_Name;
	}
	public void setItemName(String itemName) {
		this.item_Name = itemName;
	}
	public String getItemType() {
		return item_Type;
	}
	public void setItemType(String itemType) {
		this.item_Type = itemType;
	}
	public float getItemPrice() {
		return item_Price;
	}
	public void setItemPrice(float itemPrice) {
		this.item_Price = itemPrice;
	}
	
	@Override
	public String toString() {
		return "Items [itemId=" + item_Id + ", itemName=" + item_Name + ", itemType=" + item_Type + ", itemPrice="
				+ item_Price + "]";
	}
	
	

}
